#include <stdio.h>

int main() {
    float prix1, prix2, variation;

    printf("Entrez le premier prix : \n");
    scanf("%f", &prix1);

    printf("Entrez le deuxième prix : \n");
    scanf("%f", &prix2);

    if (prix1 == prix2) {
        printf("Les deux prix sont identiques, il n'y a pas de variation.\n");
    } else {
        variation = (prix2 - prix1) / prix1 * 100;
        if (variation > 0) {
            printf("Le prix a augmenté de %.2f%%.\n", variation);
        } else {
            printf("Le prix a baissé de - %.2f%%.\n", variation);
        }
    }

    return 0;
