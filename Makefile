.PHONY: all clean

make: vector squash price intlist countWord countWordBetter countChar

vector: 
 gcc -o vector vector.c

squash: 
 gcc -o squash squash.c

price: 
 gcc -o price price.c

intlist: 
 gcc -o intlist intlist.c

countWord: 
 gcc -o countWord countWord.c

countWordBetter: 
 gcc -o countWordBetter countWordBetter.c

countChar:
 gcc -o countChar countChar.c