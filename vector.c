#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main() {
	double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	double T3[5];
	int n = 5;

	printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\n ----------------------- \n");
    add_vectors(n, T1, T2, T3);
    
    for (int i = 0; i < n; i++) {
        printf("%.2Lf ", T3[i]);
    }

    double normV1 = norm_vector(n,T1);
    printf("La norme du vecteur T1 est : %.2f\n", normV1);
    return 0;
}

void add_vectors(int n, double *t1, double *t2, long double *t3) {
    for (int i = 0; i < n; i++) {
        double rslt = t1[i] - t2[i];
        t3[i] = rslt * rslt;
    }
}

double norm_vector(int n, double *t) {
    double sumSquares = 0.0;
    for (int i = 0; i < n; i++) {
        sumSquares += t[i] * t[i];
    }
    double norm = sqrt(sumSquares);
    return norm;
}
