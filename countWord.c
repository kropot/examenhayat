#include<stdio.h>
#include<stdlib.h>

int main() {
    char chaine[] = "to be or  not to be";
    int nbMot = compteNbMots(chaine);
    printf("dans la chaîne '%s',\n il y a %d mots \n",chaine, nbMot);
    return 0;
}

int compteNbMots(char *chaine){
    int nb = 0;
    int prevMots;
    while(*chaine != '\0'){
        if(*chaine == ' ' || *chaine == '\n'){
            prevMots = 0;
        }
        else if (!prevMots){
            prevMots = 1;
            nb++;
        } 
        chaine++;
    }
return nb;
}

