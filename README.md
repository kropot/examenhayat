##1
Pour supprimer un élément d'une liste chaînée en connaissant son adresse, il faut :

- Trouver le nœud qui précéde le nœud à supprimer en parcourant la liste depuis le début jusqu'à l'adresse du nœud à supprimer.
- changer le pointeur "next" du nœud précédent pour qu'il pointe sur le nœud suivant.
- faire un "free()" pour libérer la mémoire allouée

##2 
Fonction 1 "remove_list_entry_bad_taste" prend en argument la tête de la liste et l'adresse du nœud à supprimer, et ne renvoie rien. Elle parcourt la liste pour trouver le nœud précédent, met à jour le pointeur "next" et libère la mémoire. 
Fonction2 "remove_list_entry_good_taste" prend en argument un pointeur vers la tête de la liste et l'adresse du nœud à supprimer, et renvoie la nouvelle tête de la liste. 


##3
il faut que la 2e fonction renvoir une valeur car elle modifie la tête de la liste, qui peut changer si on supprime le premier nœud. C'est cette supression du 1er noeud qui engeandre le pointeur vers un pointeur

##4
La seconde fonction reçoit un pointeur vers un pointeur comme argument car elle modifie la tête de la liste, qui est un pointeur.

##5
la fonction la plus élégante est pour moi la 2e car elle permet de modifier la tête de la liste en cas de suppression du premier nœud, ce qui évite de dupliquer du code. 
De plus, l'utilisation d'un pointeur vers un pointeur permet de rendre le code plus clair et évite d'avoir à utiliser des nœuds temporaires pour parcourir la liste.
