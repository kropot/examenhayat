#include<stdio.h>
#include<stdlib.h>


void small_to_zero(int *t, int n, int val){
    for(int i = 0; i < n; i++) {
        if (t[i] <= val) {
            t[i] = 0;
        }
    }
}

int main(){
    int tab[] = {1, 3, -1, 2, 7};
    int n = 5;
    int val = 2;
    printf("Avant : ");
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }

    small_to_zero(tab, n, val);

    printf("\nAprès : ");
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }

    return 0;
}
